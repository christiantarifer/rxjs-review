import { Observer } from 'rxjs';

export const observerValue: Observer<any> = {
    next: value => console.log(value),
    error: err => console.warn(err),
    complete: () => console.log('The Observable has ended')
}