import { asyncScheduler, fromEvent } from 'rxjs';
import { throttleTime, distinctUntilChanged, pluck } from 'rxjs/operators';
import { observerValue } from '../observer';



const click$ = fromEvent<MouseEvent>(document, 'click');

click$.pipe(
    throttleTime( 3000 )
).subscribe( observerValue );

// Ejemplo 2

const input = document.createElement('input');
document.querySelector('body').append( input );

const input$ = fromEvent( input, 'keyup' );

input$.pipe(
    throttleTime( 1000, asyncScheduler, {
        leading: false,
        trailing: true,
    } ),
    pluck( 'target', 'value'),
    distinctUntilChanged()
).
subscribe( observerValue );

