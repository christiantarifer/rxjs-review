import { from, of } from "rxjs";
import { distinct, distinctUntilChanged } from "rxjs/operators";
import { observerValue } from '../observer';

const numeros$ = of<number| string>(1,'1',1,3,3,2,2,4,4,5,3,1);

numeros$.pipe(
    // distinct(),
    distinctUntilChanged( )
)
.subscribe( observerValue );

interface Personaje {
    nombre: string
}

const personajes: Personaje[] = [

    {
        nombre: 'Megaman'
    },
    {
        nombre: 'Megaman'
    },
    {
        nombre: 'X'
    },
    {
        nombre: 'Zero'
    },
    {
        nombre: 'Dr. Willy'
    },
    {
        nombre: 'X'
    },
    {
        nombre: 'X'
    },
    {
        nombre: 'Megaman'
    },
    {
        nombre: 'Zero'
    },
]

from(personajes ).pipe( 
    // distinct( p => p.nombre ),
    distinctUntilChanged( (ant, act)  => ant.nombre === act.nombre ) 
)
.subscribe( observerValue );