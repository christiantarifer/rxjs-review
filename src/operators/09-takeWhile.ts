import { fromEvent } from 'rxjs';
import { map, takeWhile } from 'rxjs/operators';


const click$ = fromEvent<MouseEvent>( document, 'click' );

click$.pipe(
    map( ({ x, y }) => ({ x,y }) ),
    // takeWhile( ( {y} ) => y <= 150  ),
    takeWhile( ( {y} ) => y <= 150, true  ),
)
.subscribe({  
    next: val => console.log('next: ', val),
    error: err => console.log('error: ', err),
    complete: () => console.log('The observer has ended.')
});
