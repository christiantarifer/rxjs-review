import { fromEvent } from 'rxjs';
import { map, tap } from 'rxjs/operators';
const texto = document.createElement('div');

texto.innerHTML = `
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eros est, dictum quis enim vitae, laoreet auctor libero. Curabitur aliquet ex id lacus tempus bibendum. Sed imperdiet rutrum neque, ac gravida ipsum vulputate eu. Morbi bibendum libero ante, eu accumsan leo posuere a. In consectetur ligula auctor sapien porttitor hendrerit eget et ligula. Mauris pretium efficitur tincidunt. Donec et egestas leo, at vehicula eros. Nullam sed est at dui cursus sollicitudin. Phasellus tempus porttitor nibh. Pellentesque vitae tellus eget ex sodales dictum.
<br/>
Curabitur quis scelerisque nulla, ut scelerisque lacus. Nullam non pharetra turpis, ut interdum enim. Fusce vel lacus non ex vestibulum iaculis. Sed semper, lacus ac facilisis sodales, elit eros venenatis eros, vitae feugiat massa lacus nec dui. Donec volutpat ac dolor sit amet pellentesque. Nam laoreet mi ut lectus dapibus, id aliquam justo commodo. Maecenas lobortis malesuada sem vitae rhoncus. Ut ante felis, rhoncus nec suscipit id, laoreet quis sem. Quisque lorem ipsum, luctus ut justo id, ullamcorper aliquam nunc. Etiam ut eleifend odio. Cras placerat iaculis eros, ultricies varius eros lacinia sed.
<br/>
Etiam fermentum ante nisi, ultricies aliquam massa placerat non. Phasellus ut sem velit. Integer sodales ipsum id elit finibus, in tincidunt erat tristique. Integer facilisis augue et auctor volutpat. Curabitur congue mauris at interdum consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus eu consectetur est. Suspendisse in felis blandit, sodales magna sed, vestibulum nulla. Phasellus vestibulum erat id interdum consectetur. Nulla semper libero vitae quam cursus congue. Vivamus imperdiet lacinia dui, non fermentum odio placerat non. Donec ut lacus sem. Aenean laoreet eget sapien in consectetur. Suspendisse tempor eros ac condimentum laoreet.
<br/>
Nullam aliquet erat nec sapien posuere vulputate. Vivamus porttitor, urna id commodo tempor, magna dui tincidunt tortor, at rhoncus diam lacus eget nunc. Vestibulum accumsan, nulla condimentum suscipit efficitur, ante nibh dignissim nisl, laoreet tristique elit turpis eget nisi. Nam sit amet dictum metus, ut iaculis massa. Quisque interdum, neque quis luctus tristique, leo augue facilisis erat, ac cursus ante purus quis lacus. Integer at faucibus mi. Curabitur rhoncus, arcu quis auctor condimentum, quam ipsum sollicitudin sem, at sollicitudin tortor nulla a erat. Integer vel eros urna.
<br/>
Etiam sit amet urna eleifend, dapibus sem ut, fermentum dui. Donec auctor sed augue eu elementum. Nullam molestie, magna et malesuada rhoncus, elit leo euismod augue, aliquet posuere nunc tortor ac turpis. Nullam bibendum nulla at est accumsan bibendum. Suspendisse fringilla eget velit at lacinia. In fringilla arcu et dui porta, id ornare velit sagittis. Morbi non neque eros. Nam dui ante, mollis ut dictum ornare, bibendum ut ex. Suspendisse non risus mi. Cras vel ipsum et eros venenatis elementum. Praesent ullamcorper tellus sapien, tempor maximus lectus ornare id. Ut ac iaculis leo, id pulvinar diam.
<br/>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec arcu tellus, ultricies ut odio id, semper pharetra arcu. Maecenas vitae enim vel arcu aliquam tincidunt non eget lacus. Donec ut velit ac odio iaculis pulvinar. Proin vel ex fermentum, viverra ante eu, fermentum nibh. Nulla facilisi. Curabitur vitae nunc varius, blandit est ac, consequat eros. Mauris eget cursus elit, vel varius turpis. Proin facilisis mattis vehicula. Mauris sit amet tincidunt nunc. Suspendisse iaculis pretium dictum. Integer ante orci, laoreet ac ligula id, tempor consectetur tellus. In pretium magna vel efficitur scelerisque. Nulla fermentum placerat lorem sed vehicula.
<br/>
Aliquam et lectus felis. Phasellus ut dignissim ipsum, et eleifend enim. Vivamus sed semper nunc, nec varius ex. Praesent sed bibendum est. Donec a ante luctus, bibendum risus et, faucibus ante. Donec tempor lorem sit amet sem rutrum feugiat. Phasellus viverra justo id ex viverra, non posuere orci lobortis.
<br/>
Duis et mattis purus. Donec molestie eu tellus sed consectetur. Proin aliquet pretium lectus vehicula vulputate. Nullam enim sapien, fermentum at maximus sed, vulputate sed sem. Ut tristique pulvinar lectus, eget suscipit nulla malesuada ac. Aliquam dapibus nisi non congue euismod. In eleifend libero ipsum, id iaculis sem luctus vitae. Aenean ut semper tortor. Aenean ut nunc et urna hendrerit faucibus.
<br/>
Aliquam luctus velit justo, non eleifend augue pulvinar nec. Morbi sit amet nisl ultrices, fermentum ante eu, volutpat leo. Suspendisse potenti. Aenean at neque nec dui malesuada tincidunt quis id turpis. Praesent a pulvinar massa, sed auctor dui. Vivamus dolor tellus, dapibus feugiat suscipit vel, iaculis sed turpis. Aenean augue sem, tincidunt vitae purus id, efficitur egestas lacus. Sed volutpat sapien sed turpis lobortis, eget dapibus ex vehicula. Nulla nec tincidunt urna. Sed at sagittis augue, nec posuere mi. Nam semper ligula non libero tincidunt semper. Aliquam sagittis porttitor felis sed scelerisque.
<br/>
Praesent a tristique tellus. Nulla pharetra, ligula eget gravida semper, ligula ipsum volutpat augue, sed maximus arcu tortor quis lacus. Sed rutrum enim ex, eget vulputate nunc euismod eget. Quisque vulputate, lectus varius ornare eleifend, nisl ligula volutpat magna, sed mollis enim libero ac urna. Ut non diam ligula. Etiam ultrices consectetur ante, vulputate mollis nibh ornare vitae. Nam eleifend laoreet dolor vitae pretium. In efficitur ornare eros, nec facilisis turpis luctus at. Aliquam erat volutpat. Ut dictum rutrum risus, a interdum orci accumsan vitae.
<br/>
Suspendisse eget nunc interdum, volutpat orci vitae, feugiat ipsum. Nam vel fringilla sem. Nullam semper, nulla id iaculis feugiat, dolor arcu pharetra libero, vel dictum diam sem vitae erat. In lacinia mollis placerat. Donec euismod nibh vel sagittis ultricies. Vivamus consequat et mauris vel congue. Vivamus ex justo, dictum quis rhoncus ac, sagittis sit amet magna. In a ullamcorper eros, sed tincidunt erat. Vivamus eget lorem nec nibh euismod condimentum. Etiam finibus tellus nec nibh semper, at volutpat urna malesuada.
<br/>
Vestibulum suscipit velit arcu, ac convallis lorem tristique sed. Cras nunc nulla, aliquet vehicula aliquet fermentum, suscipit eu purus. Phasellus porta eget diam a vestibulum. Aliquam massa neque, elementum et massa eget, pulvinar lobortis dui. Vivamus tincidunt auctor orci, vitae gravida velit malesuada elementum. Duis diam nisi, lacinia at laoreet et, aliquam sit amet leo. Maecenas non viverra justo. In non dapibus massa. In leo nibh, convallis at bibendum nec, aliquet eget metus. Integer ultricies orci odio, quis cursus urna pretium nec. Praesent pellentesque, quam sit amet commodo rutrum, turpis lorem dictum sapien, non congue diam nisl tempor urna. Donec mattis eros sed sagittis ultrices. Morbi scelerisque at massa in convallis. Vivamus convallis libero vel lacinia volutpat.
<br/>
Sed congue consectetur eros in fringilla. Quisque vestibulum quam ex, ac efficitur enim pellentesque tincidunt. Aenean ultrices nibh sit amet diam pretium vestibulum. Vestibulum eleifend suscipit augue, a consequat sapien vulputate id. Fusce rutrum metus eros, in dapibus risus tempus et. Aliquam ornare, massa a pulvinar tempor, lectus dolor tempus odio, nec feugiat lorem nunc quis purus. Nulla facilisi. Suspendisse placerat aliquet molestie. Vestibulum suscipit eget elit in auctor. Proin non augue eu metus vehicula sollicitudin vel non quam. Fusce sollicitudin venenatis euismod. Phasellus iaculis augue aliquam dolor fermentum dictum. Phasellus a lacus ut turpis efficitur ullamcorper vel eu lorem. Nam a erat libero.
<br/>
Maecenas lacinia lectus id neque condimentum, vitae efficitur quam sagittis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque condimentum sed orci sed ullamcorper. Pellentesque egestas nec sapien ac tempor. Quisque sollicitudin accumsan nibh rhoncus ultricies. Nunc nisi tortor, tempus sit amet nunc a, pretium blandit nibh. In sagittis felis vel ante sodales feugiat vitae sit amet risus. Curabitur ut rutrum felis. Sed fringilla sem at consequat sagittis. Aliquam justo mi, faucibus eu ullamcorper sit amet, congue et lacus. Phasellus non pellentesque turpis. Fusce pharetra molestie posuere. Sed elit augue, convallis nec purus id, ultricies ullamcorper eros. Donec in felis tincidunt, lobortis sapien a, maximus arcu.
<br/>
Donec eleifend eu justo eget pellentesque. Donec dictum elementum ligula vel vehicula. Morbi blandit dolor nibh, eu egestas quam eleifend et. Nullam tempus enim ut interdum porttitor. Donec et leo eget odio rhoncus cursus at non nisl. Praesent purus metus, sollicitudin vitae tincidunt ut, faucibus at neque. Fusce dignissim et felis ac hendrerit.
<br/>
Nullam a lectus tempus, posuere nibh non, imperdiet tellus. Donec consectetur neque sit amet auctor hendrerit. Fusce sed nisl interdum, facilisis urna ac, consequat ipsum. Donec rhoncus quam quis nisl scelerisque, sit amet faucibus tortor sodales. Aliquam in sem leo. Sed nisl mauris, vestibulum tempor elementum vel, finibus at sapien. Vestibulum tincidunt facilisis libero, non auctor elit dignissim nec. Proin vel justo ultricies, tristique metus at, imperdiet enim. In hac habitasse platea dictumst. Suspendisse auctor ligula vel orci egestas aliquam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi molestie, est ac dignissim placerat, turpis felis malesuada orci, commodo molestie purus velit eget ipsum. Aliquam elementum odio at augue aliquam, sit amet blandit lacus feugiat. Aliquam pretium erat orci, vitae feugiat nisl eleifend sed. Integer id faucibus odio. Nullam a enim non massa tempor fermentum.
<br/>
Cras non turpis cursus, imperdiet nulla vitae, efficitur felis. Curabitur efficitur at quam sed pulvinar. Vestibulum varius at enim vel pretium. Phasellus vel porttitor ligula. Fusce ut viverra nunc. Suspendisse eu auctor velit. Interdum et malesuada fames ac ante ipsum primis in faucibus.
<br/>
Vivamus vel mattis arcu. Suspendisse dictum, est condimentum cursus suscipit, orci eros porttitor elit, ultrices euismod ante urna feugiat diam. Nulla ac aliquet ligula, ac condimentum leo. Sed id luctus ligula. Fusce bibendum metus ut eros molestie, ac vestibulum velit pharetra. Nunc euismod massa ac arcu aliquam, a pellentesque risus egestas. Nam nec tempus turpis. Proin lectus nibh, sodales id libero vel, tincidunt pulvinar nibh. Pellentesque mollis magna tellus, non aliquam elit ullamcorper eget.
<br/>
Ut maximus ultrices arcu, eget aliquet ipsum fermentum sed. Aliquam erat volutpat. Donec mollis pellentesque accumsan. Suspendisse sit amet hendrerit neque, non aliquet diam. Nulla faucibus magna vitae arcu viverra, quis mattis metus aliquam. Phasellus lobortis tincidunt sapien placerat vestibulum. Duis iaculis nisi quis diam facilisis, eget molestie purus interdum. Donec eget metus posuere odio scelerisque sollicitudin. Sed efficitur tellus a risus ultricies posuere. Curabitur suscipit metus iaculis, pellentesque urna at, aliquam odio.
<br/>
Quisque accumsan, ipsum ac suscipit pharetra, sem elit ultricies magna, eu venenatis mauris nibh vel ex. Aliquam convallis, massa quis vulputate lacinia, velit augue gravida ligula, eget mattis sapien nisi a ex. Nulla vitae augue consectetur, facilisis lorem eu, faucibus lorem. Vestibulum nec tellus pretium, pretium felis suscipit, tempor erat. Aliquam condimentum eros ligula, quis dapibus magna gravida vel. Maecenas gravida turpis nec quam placerat, sit amet venenatis leo tempus. Etiam aliquam gravida ultricies. Nam nec suscipit sapien, at faucibus nibh.
<br/>
Praesent elementum malesuada felis, ut tristique nibh mattis sed. Donec aliquet accumsan leo. Phasellus aliquam lacus in molestie pharetra. Donec euismod tempor odio eu volutpat. Suspendisse hendrerit efficitur enim, vel venenatis nulla dignissim tincidunt. Integer porttitor, lacus ut vehicula pretium, neque dolor mattis arcu, quis sagittis diam ante eu nisi. Morbi sodales tortor ut nulla egestas, eget fermentum nunc scelerisque. Suspendisse volutpat mattis turpis a tempor. Cras tincidunt pellentesque dictum.
<br/>
Proin ornare augue sed est vulputate vulputate. Curabitur faucibus lacinia blandit. In semper commodo porta. Integer eget ullamcorper enim. Morbi bibendum est ut orci laoreet, lobortis euismod nisl dapibus. Fusce ut porta mi. Suspendisse non arcu et turpis gravida euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed rhoncus lectus ut est rutrum, vel varius ipsum rhoncus. Proin nibh est, facilisis a posuere quis, rutrum vitae justo. Nunc eu purus sem. Integer eu orci vulputate quam rutrum efficitur eu eu mauris. Morbi id ligula magna. Maecenas suscipit tempus lectus, eget consequat enim condimentum ut. Curabitur eleifend libero ipsum, et facilisis risus condimentum a. Proin faucibus erat eu justo ultrices consectetur.
<br/>
Duis feugiat faucibus interdum. Aliquam efficitur augue diam, non sagittis ante facilisis at. Aenean interdum, nunc at vulputate vulputate, tortor dolor varius metus, vel luctus nisl mi vitae leo. Nunc sed imperdiet ante. Sed cursus est vitae consequat blandit. Donec et consectetur quam. Fusce convallis molestie nisi laoreet ultrices. Nunc et mattis libero, et interdum nulla. Maecenas vitae vehicula ex, non aliquet tortor. Integer tortor justo, blandit ac nulla vitae, varius rutrum leo. Curabitur in scelerisque ante. Duis porttitor euismod libero, at varius arcu sollicitudin eu. Pellentesque id ultrices quam. Vivamus eu faucibus purus. Nulla sed euismod augue, ac ornare mi.
<br/>
Ut lacus justo, pretium ut nunc ut, varius varius tellus. Mauris quam massa, molestie at suscipit eget, vulputate non libero. Pellentesque vehicula efficitur dolor, vel tristique diam dapibus dictum. Pellentesque at feugiat urna, sed ultricies nulla. Donec orci neque, commodo facilisis tortor vel, viverra lobortis ante. In tincidunt, ipsum lacinia faucibus fringilla, nisi quam malesuada nulla, lobortis mattis tellus purus sodales nulla. Phasellus eget fermentum mi. Nunc dictum quam ut leo porta volutpat. In nec mauris volutpat, consectetur nisl dictum, tempor ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
<br/>
Fusce imperdiet felis sem, vitae elementum odio fermentum id. Vivamus rhoncus libero eget quam consequat, vel pellentesque felis pretium. Curabitur aliquam, dolor at tincidunt finibus, diam risus tempor lectus, vitae rhoncus dolor odio quis tellus. Nullam a vehicula dolor. Praesent fermentum augue nec arcu faucibus, sed scelerisque mi sodales. Proin porttitor vulputate turpis nec euismod. Pellentesque convallis fermentum feugiat. Maecenas non massa sit amet tortor vehicula auctor. Vestibulum quis ex condimentum, mattis ipsum ac, scelerisque ligula.
<br/>
`;

const body = document.querySelector('body');

body.append( texto );

const progressBar = document.createElement('div');

progressBar.setAttribute('class', 'progress-bar');

body.append(progressBar);

// * FUNCTION TO CALCULATE
const calcularPorcentajeScroll = ( event ) => {

    const { scrollTop, scrollHeight, clientHeight } = event.target.documentElement;

    // console.log( { scrollTop, scrollHeight, clientHeight } );

    return ( scrollTop / ( scrollHeight - clientHeight ) ) * 100;

}


// * STREAMS
const scroll$ = fromEvent( document, 'scroll' );

// scroll$.subscribe( console.log );

const progres$ = scroll$.pipe(

    // map( event => calcularPorcentajeScroll(event) ),

    map( calcularPorcentajeScroll ),
    tap( console.log )

);

progres$.subscribe( porcentaje => {

    progressBar.style.width = `${porcentaje}%`

})