import { from, Subscriber } from 'rxjs';
import { reduce, scan, map } from 'rxjs/operators';
const numeros = [1, 2, 3, 4, 5]

// const totalAcumulador = ( acc, cur ) => { 
//     return acc + cur
//  }

 const totalAcumulador = ( acc, cur ) => acc + cur

//  * REDUCE
from( numeros ).pipe(
    reduce( totalAcumulador, 0 )
)
.subscribe( console.log )

//  * SCAN
from( numeros ).pipe(
    scan( totalAcumulador, 0 )
)
.subscribe( console.log )

// * REDUX
interface Usuario{
    id?: string;
    autenticado?: boolean;
    token?: string;
    edad?: number;
}

const user: Usuario[] = [
    { id: 'Chris', autenticado: false, token: null },
    { id: 'Chris', autenticado: false, token: 'ABC' },
    { id: 'Chris', autenticado: false, token: 'ABC123' },
];

const state$ = from(user).pipe( 
    scan<Usuario>( (acc, cur) => {
        return { ...acc, ...cur }
    }, { edad: 28 } )
 )


const id$ = state$.pipe( map( state => state.id ) );

id$.subscribe( console.log )
