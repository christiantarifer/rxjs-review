import { range, fromEvent } from 'rxjs';
import { map, mapTo, pluck } from 'rxjs/operators';

range(1, 5).pipe(map<number, string>( val => (val * 10).toString() )).subscribe( console.log );

const keyUp$ = fromEvent<KeyboardEvent>( document, 'keyup' );

//  * map()
const keyUpCode$ = keyUp$.pipe( map( event => event.code ) );

//  * pluck()
const keyUpPluck$ = keyUp$.pipe( pluck( 'target', 'baseURI' ));

//  * mapTo()
const keyUpMapTo$ = keyUp$.pipe( mapTo( 'teclaPresionada' ));


//  * PRINT VALUES
keyUp$.subscribe( console.log );

keyUpCode$.subscribe( code => console.log('map', code) );

keyUpPluck$.subscribe( code => console.log('pluck', code) );

keyUpMapTo$.subscribe( code => console.log('mapTo', code) );