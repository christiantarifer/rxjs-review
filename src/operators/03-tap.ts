import { range } from 'rxjs';
import { tap, map } from 'rxjs/operators';

const numeros$ = range(1, 5);


numeros$.pipe(
    tap( val => console.log(`before ${val}`) ),
    map( val => val * 10 ),
    tap( {
        next: valor => console.log('after', valor),
        complete: () => console.log('It is over')
    } )
)
.subscribe( val => console.log('subs', val) );