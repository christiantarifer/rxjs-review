import { interval, fromEvent } from 'rxjs';
import { skip, takeUntil, tap } from 'rxjs/operators';
import { observerValue } from '../observer';


const boton = document.createElement('button');

boton.innerHTML = 'Detener timer';

document.querySelector('body').append( boton );

const counter$ = interval(1000);
// const clickBtn$ = fromEvent( boton, 'click' );
const clickBtn$ = fromEvent( boton, 'click' ).pipe(
    tap( () => console.log('Tap antes de skip') ),
    skip(1),
    tap( () => console.log('Tap despues de skip') ),
)

counter$.pipe(
    takeUntil( clickBtn$ )
).subscribe( observerValue )