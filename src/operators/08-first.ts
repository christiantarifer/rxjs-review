import { fromEvent } from 'rxjs';
import { first, take, tap, map } from 'rxjs/operators';

const click$ = fromEvent<MouseEvent>( document, 'click' );

click$.pipe(
    tap<MouseEvent>( console.log ),
    map( ( { clientX, clientY } ) => ({
        clientY,
        clientX
    }) 
    ),
    first( event => event.clientY >= 150 )
)
.subscribe({
    next: val => console.log(val),
    error: err => console.log(err),
    complete: () => console.log('The Observable has ended')
});