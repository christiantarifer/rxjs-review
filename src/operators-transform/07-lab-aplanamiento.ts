import { fromEvent, of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { tap, map, mergeMap, pluck, catchError, switchMap, exhaustMap } from 'rxjs/operators';

// HELPER
const peticionHttpLogin = ( userPass ) => 
                            ajax.post('https://reqres.in/api/login?delay=1', userPass)
                                    .pipe( 
                                        pluck( 'response', 'token' ),
                                        catchError( err => of( `There was an error on the credentials : ${err.response.error}` ) )
                                    );

// CREATE FORM
const form = document.createElement('form');

const inputEmail = document.createElement('input');
const inputPass = document.createElement('input');
const submitBtn = document.createElement('button');

// SET UP TO FIELDS
inputEmail.type = 'email';
inputEmail.placeholder = 'Email';
inputEmail.value = 'eve.holt@reqres.in';

inputPass.type = 'password';
inputPass.placeholder = 'Password';
inputPass.value = 'cityslicka';

submitBtn.innerHTML = 'Ingresar';

// ADD ELEMENTS TO THE FORM
form.append( inputEmail, inputPass, submitBtn );

// ADD FORM TO THE HTML
document.querySelector('body').append( form );

// STREAMS
const submitForm$ = fromEvent<Event>( form, 'submit' ).pipe( 
                            tap( ev => ev.preventDefault() ),
                            map( ev => ({
                                email: ev.target[0].value,
                                password: ev.target[1].value
                            }) 
                            ),
                            // mergeMap( peticionHttpLogin ) -> Executes all the request
                            //switchMap( peticionHttpLogin ) -> Executes only the last request and cancells previous request
                            exhaustMap( peticionHttpLogin )
                    );

submitForm$.subscribe( token => console.log(token) );