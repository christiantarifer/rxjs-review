import { Observable, Observer, Subscriber } from "rxjs";


// * LESS COMMON WAY TO CREATE AN OBSERVABLE
// const obs$ = Observable.create()

// * Observer<type> --> An interface that defines a Observable
const observer: Observer<string>  = {

    next: value => console.log(`Siguiente [next] : ${value}`),

    error: error => console.warn(`Error [error]: ${error}`),

    complete: () => console.info('The observer is completed')

} 

const obs$ = new Observable<string> ( subs => {

    // * EMMIT VALUES
    subs.next('Hola, has venido sola, ')
    subs.next('o estas de pasadita? ')

    subs.next(' Se nota que no eres de por acá')

    // * FORCE AN ERROR
    // const a = undefined
    // a.nombre = 'Gaaaa'

    // * STOP EMMITING
    subs.complete();

    subs.next('Gaaaaaa')

});


// * OLD FORM
// obs$.subscribe( resp => console.log(resp) );

// obs$.subscribe( resp => {

//     console.log(resp);

// });

// * EMC6 STANDAR
// obs$.subscribe( console.log );


// obs$.subscribe( 

//     // * EMMIT A VALUE
//     valor => console.log(`next : ${valor}`),

//     // * WHEN AN ERROR OCCURS
//     error => console.warn(`error : ${error}`),

//     // * EMMITED WHEN COMPLETED
//     () => console.info('Completado')

// );

obs$.subscribe( observer );