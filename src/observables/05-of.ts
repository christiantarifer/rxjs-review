import { of } from "rxjs";

// const obs$ = of(1, 2, 3, 4, 5, 6);

// const obs$ = of(...[1, 2, 3, 4, 5, 6],7,8,9);

// const obs$ = of([1, 2], {a:1, b:2}, function(){}, true, Promise.resolve(true));

const obs$ = of<number>(1, 2, 3, 4, 5, 6);

obs$.subscribe( 
    next => console.log(`next: ${next}`),
    error => console.log(`error: ${error}`),
    () => console.log('The secuence has ended')
    )

console.log('End of obs$');