import { Observable, Observer, Subscriber } from "rxjs";


// * LESS COMMON WAY TO CREATE AN OBSERVABLE
// const obs$ = Observable.create()

// * Observer<type> --> An interface that defines a Observable
const observer: Observer<any>  = {

    next: value => console.log(`next : ${value}`),

    error: error => console.warn(`error: ${error}`),

    complete: () => console.info('The observer is completed')

} 

const intervalo$ = new Observable<number>( subscriber => {

    // * CREATE A COUNTER 1, 2, 3, 4, 5,
    let count = 0;

    // * FUNCTIONALITY
    const interval = setInterval(() => {
        
        count++;
        subscriber.next( count );
        // console.log( count );

    }, 1000)

    setTimeout(() => {
        
        subscriber.complete()

    }, 2500);


    // * STOP FUNCTIONALITY
    return () => {

        clearInterval( interval );
        console.info('Intervalo destruido');

    }

});

const subs1 =  intervalo$.subscribe( observer );
const subs2 =  intervalo$.subscribe( observer );
const subs3 =  intervalo$.subscribe( observer );

// * STACK N OBSERVABLES TO A MAIN ONE
subs1.add( subs2 )
    .add( subs3 );
    
    
setTimeout(() => {

    // * STOP LISTENING TO THE OBSERVABLE
    subs1.unsubscribe();

    console.log('Completado timeout');

}, 6000 );
