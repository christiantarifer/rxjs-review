import { observable, Observable, Observer, Subject, Subscriber, Subscription } from "rxjs";


// * LESS COMMON WAY TO CREATE AN OBSERVABLE
// const obs$ = Observable.create()

// * Observer<type> --> An interface that defines a Observable
const observer: Observer<any>  = {

    next: value => console.log(`next : ${value}`),

    error: error => console.warn(`error: ${error}`),

    complete: () => console.info('The observer is completed')

} 

const intervalos$ = new Observable<number>( subs => {

    const intervalID = setInterval( () => 
        subs.next( Math.random() ), 3000 
        
    )

    return () => {
        clearInterval( intervalID );
        console.log('Intervalo destruido');
    }

});

/**
 * * SUBJECT
 * 1) MULTI CASTING --> MULTIPLE CHANNELS CAN CONSUME THE SAME ELEMENTS
 * 2) IT IS AN OBSERVER --> IT HAS next, error and complete properties
 */

const subject$ = new Subject();

const subscription = intervalos$.subscribe( subject$ );

// const subs1 = intervalos$.subscribe( rnd => console.log(`Subs1 : ${rnd}`) )
// const subs2 = intervalos$.subscribe( rnd => console.log(`Subs2 : ${rnd}`) )

// const subs1 = subject$.subscribe( rnd => console.log(`Subs1 : ${rnd}`) )
// const subs2 = subject$.subscribe( rnd => console.log(`Subs2 : ${rnd}`) )

const subs1 = subject$.subscribe( observer )
const subs2 = subject$.subscribe( observer )

setTimeout(() => {
    
    subject$.next(10);

    subject$.complete();

    subscription.unsubscribe();

}, 3500);